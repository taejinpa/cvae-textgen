
model_dir=`ls "$PWD"/bin -t | head -1`

epoch=$1
NUM_SAMPLES=$2
COND1=$3
ALPHA=$4
TOPN=$5
SEED=$6


if [ -z "$1" ]; then
    epoch=3
fi
if [ -z "$2" ]; then
    NUM_SAMPLES=10
fi
if [ -z "$3" ]; then
    COND1=3
fi
if [ -z "$4" ]; then
    ALPHA=0.5
fi
if [ -z "$5" ]; then
    TOPN=50
fi
if [ -z "$6" ]; then
    SEED=1
fi

DATA_DIR=$HOME/amazon_data
CHECKPOINT=$PWD/bin/"$model_dir"/E"$epoch".pytorch
#CHECKPOINT=/home/inctrl/projects/cvae-textgen/bin/2019-Nov-21-07:26:31/E11.pytorch
echo MODEL_DIR: $model_dir
echo CHECKPOINT : $CHECKPOINT
echo NUM_SAMPLES: $NUM_SAMPLES
echo COND1: $COND1
python3 inference.py -c $CHECKPOINT -n $NUM_SAMPLES -cond1 $COND1 -seed $SEED -dd $DATA_DIR --alpha $ALPHA --topn $TOPN 
