import os
import json
import torch
import argparse
import ipdb
from model import SentenceVAE
from utils import to_var, idx2word, interpolate

import numpy as np

def disp_sentence(goo):
    sentiment = []
    for line in goo:
        if any(x in line for x in [ "broke", "broken", "defective", "horrible", "disappointed", "return", "bad", "not", "doesn't", "don't", "worst", "junk", "crap"]):
            sentiment.append("1")
        elif any(x in line for x in ["perfectly", "good", "glad", "great", "nice", "happy","loves", "love", "best"]):
            sentiment.append("5")
        else:
            sentiment.append("?")
    total = list(zip(sentiment, goo))
    print(*total, sep='\n')
    print( " cond:{} -- {} negative {} neutral {} positive -- total {}".format(args.cond1, 
                                                         np.count_nonzero(np.array(sentiment) == "1"),  
                                                         np.count_nonzero(np.array(sentiment) == "?"),  
                                                         np.count_nonzero(np.array(sentiment) == "5"), len(goo)))

def main(args):

    with open(args.data_dir+'/amazon.vocab.json', 'r') as file:
        vocab = json.load(file)

    w2i, i2w = vocab['w2i'], vocab['i2w']

    model = SentenceVAE(
        vocab_size=len(w2i),
        sos_idx=w2i['<sos>'],
        eos_idx=w2i['<eos>'],
        pad_idx=w2i['<pad>'],
        unk_idx=w2i['<unk>'],
        max_sequence_length=args.max_sequence_length,
        embedding_size=args.embedding_size,
        rnn_type=args.rnn_type,
        hidden_size=args.hidden_size,
        word_dropout=args.word_dropout,
        embedding_dropout=args.embedding_dropout,
        latent_size=args.latent_size,
        target_sig=args.target_sig,
        num_layers=args.num_layers,
        bidirectional=args.bidirectional
        )

    if not os.path.exists(args.load_checkpoint):
        raise FileNotFoundError(args.load_checkpoint)

    model.load_state_dict(torch.load(args.load_checkpoint))
    print("Model loaded from %s"%(args.load_checkpoint))

    if torch.cuda.is_available():
        model = model.cuda()
    
    model.eval()
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    print('----------SAMPLES----------')
    m = 8
    n = m ** 2 
    # samples, refined_z = model.inference(n=n*args.num_samples, cond1=args.cond1, margin=n, refining=True)
    # goo = idx2word(samples, i2w=i2w, pad_idx=w2i['<pad>'])
    # disp_sentence(goo) 
    # tr = float(1/m)
    # samples, z = model.inference(n=refined_z.shape[0], z=refined_z, cond1=args.cond1, dsc_trim_ratio=tr, refining=False)
   
    tr = 0.1
    samples, z = model.inference(n=int(args.num_samples/tr), cond1=args.cond1, dsc_trim_ratio=tr, 
                                refining=False, topn=args.topn, alpha=args.alpha)
    foo = idx2word(samples, i2w=i2w, pad_idx=w2i['<pad>'])
    disp_sentence(foo)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    
    dim_all = 512
    parser.add_argument('-cond1', '--cond1', type=float)
    parser.add_argument('-seed', '--seed', type=int)
    parser.add_argument('-c', '--load_checkpoint', type=str)
    parser.add_argument('-n', '--num_samples', type=int, default=10)
    parser.add_argument('-dd', '--data_dir', type=str, default='amazon_data')
    parser.add_argument('-ms', '--max_sequence_length', type=int, default=30)
    parser.add_argument('-eb', '--embedding_size', type=int, default=300)
    parser.add_argument('-rnn', '--rnn_type', type=str, default='gru')
    parser.add_argument('-hs', '--hidden_size', type=int, default=dim_all)
    parser.add_argument('-wd', '--word_dropout', type=float, default=0)
    parser.add_argument('-ed', '--embedding_dropout', type=float, default=0.5)
    parser.add_argument('-ls', '--latent_size', type=int, default=2)
    parser.add_argument('-nl', '--num_layers', type=int, default=1)
    parser.add_argument('-bi', '--bidirectional', action='store_true')
    parser.add_argument('-ts', '--target_sig', type=float, default=0.1)
    parser.add_argument('-a', '--alpha', type=float, default=0.5)
    parser.add_argument('-tn', '--topn', type=int, default=50)
    
    args = parser.parse_args()
    # args.data_dir = "/raid0_8tb/datasets/amazon_review_data/toy_data2"
    args.data_dir = "/home/inctrl/amazon_data"

    args.rnn_type = args.rnn_type.lower()

    assert args.rnn_type in ['rnn', 'lstm', 'gru']
    assert 0 <= args.word_dropout <= 1

    main(args)
