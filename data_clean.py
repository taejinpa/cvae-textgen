# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import re
import ipdb
import os
from pathlib import Path
import argparse
# reload(sys)
os.environ["PYTHONIOENCODING"] = "utf-8"

def write_txt(path, the_list): 
    outF = open(path, "w")
    for line in the_list:
        outF.write(line)
        outF.write("\n")
    outF.close()


def read_txt(list_path):
    with open(list_path, encoding="utf-8") as f:

        content = []
        for line in f:
            line = line.encode('ascii', 'ignore').decode('ascii')
            line = line.strip("\n")
            # print(line) 
            content.append(line)
    return content

def get_split_key(split_dict, k):
    if 0 <= k < split_dict["train"]:
        return "train"
    elif split_dict["train"] <= k < split_dict["valid"]:
        return "valid"
    elif split_dict["valid"] <= k < split_dict["test"]:
        return "test"
    else:
        raise ValueError("{} does not belong to any category.".format(k))

def text_clean(line, unwanted):
    # line = line.encode('utf-8').decode('utf-8', 'ignore')
    line = line.replace("<br />", "")

    puncs = [".", ",", "!", "/"]
    for punc in puncs:
        line = line.replace(punc, " {} ".format(punc))
    # print(line)
    resultwords = line.lower().split()
    # resultwords  = ([word.lower() for word in re.split("\W+",line) if word.lower() not in unwanted])

    return ' '.join(resultwords), len(resultwords)
    # line."<br />"

def get_col_dict(cont, dict_out, split_dict, item_names_list, unwanted):
    cont_col_names = cont[0]
    dict_keys = cont_col_names.split("\t")
    cont_body = cont[1:]

    count = {"train": 0, "test": 0, "valid": 0}
    for k, line_raw in enumerate(cont_body):
        if (k) % int(len(cont_body)/10) == 0:
            print("Reading {}/{}".format(k, len(cont_body)))
        
        line = line_raw.split("\t")
        rev_body_len = len( line[dict_keys.index("review_body")])
        ### if minmax_sent_len[0] <= rev_body_len <= minmax_sent_len[1]:
        split_key = get_split_key(split_dict, k)

        for i, item_names_tup in enumerate(item_names_list):
            (key, data_type) = item_names_tup
            assert key in dict_keys, "The item name {} is not in the data".format(key)
            idx = dict_keys.index(key)
            line_item, line_length = text_clean(line[idx], unwanted)
            if key not in dict_out:
                print("Creating key:", key)
                dict_out[key] = {split_key: [ line_item ] }
            else:
                if split_key not in dict_out[key]:
                    dict_out[key][split_key] = [ line_item ]
                else:
                    dict_out[key][split_key].append(line_item)
    return dict_out


if __name__ == "__main__":
    '''
    Data cleaning script.

    ex: python3 data_clean --data_dir  ~/data_folder
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str)
    args = parser.parse_args() 

    home_path = str(Path.home())
   
    data_base_path = args.data_dir
    dataset_list = ["amazon_reviews_us_Musical_Instruments_v1_00.tsv"]
    full_tsv_file_path_list = [ "{}/{}".format(data_base_path, x) for x in dataset_list ]
    full_tsv_file_out_path = data_base_path
    
    print("data_base_path: {}".format(data_base_path))
    print("full_tsv_file_out_path:{}".format(full_tsv_file_out_path))
   
    ### marketplace customer_id review_id   product_id  product_parent  product_title   product_category    star_rating helpful_votes   total_votes vine    verified_purchase   review_headline review_body review_date
    item_names_list = [("review_body", ""), 
                       ("star_rating", ".star_rating"),
                       ("product_title", ".product_title") ]

    
    total_train = int(1e+5)
    truncate_len = total_train
    val_ratio, test_ratio = 0.1, 0.1

    # minmax_sent_len = (5, 60)
    itemized_data_dict = {}
    for full_tsv_file_path in full_tsv_file_path_list:
        data_tag = full_tsv_file_path.split("/")[-1].split("_")[0]
        cont = read_txt(full_tsv_file_path) 

        cont = cont[:(truncate_len+1)]
       
        split_dict = {"valid": int(len(cont) * (1-test_ratio)),
                      "test": int(len(cont)),
                      "train": int(len(cont) * (1-val_ratio-test_ratio)  )}
        
        print("Adding dataset:", split_dict)

        ### Specify the unwanted strings 
        unwanted = ["<br />"]

        ### Organize the dataset into "itemized_data_dict"
        # itemized_data_dict = get_col_dict(cont, itemized_data_dict, max_line, minmax_sent_len, item_names_list, split, unwanted)
        itemized_data_dict = get_col_dict(cont, itemized_data_dict, split_dict, item_names_list, unwanted)
   
    # ipdb.set_trace()
    print("Writing files")
    for (item_name, data_type) in item_names_list:
        for split_name in ["train", "valid", "test"]:
            write_txt("{}/{}".format(full_tsv_file_out_path, "{}.{}{}.txt".format(data_tag, split_name, data_type)), 
                      itemized_data_dict[item_name][split_name])
