import torch
import torch.nn as nn
import torch.nn.utils.rnn as rnn_utils
from utils import to_var
import ipdb
import numpy as np

import random
from utils import to_var, idx2word, interpolate


class SentenceVAE(nn.Module):

    def __init__(self, vocab_size, embedding_size, rnn_type, hidden_size, word_dropout, embedding_dropout, latent_size,
                sos_idx, eos_idx, pad_idx, unk_idx, max_sequence_length, target_sig, num_layers=1, bidirectional=False):

        super().__init__()
        self.tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.Tensor
        
        self.target_sig = torch.tensor(target_sig, dtype=torch.float).cuda()
        self.target_sig.requires_grad = False

        self.max_sequence_length = max_sequence_length
        self.sos_idx = sos_idx
        self.eos_idx = eos_idx
        self.pad_idx = pad_idx
        self.unk_idx = unk_idx

        self.latent_size = latent_size

        self.rnn_type = rnn_type
        self.bidirectional = bidirectional
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.vocab_size = vocab_size
        self.teacher_forcing_ratio = 1.0
        self.embedding_size = embedding_size

        self.embedding = nn.Embedding(vocab_size, embedding_size)
        self.embedding = nn.Embedding(vocab_size, embedding_size)
        self.cond1_size = 1
        self.word_dropout_rate = word_dropout
        self.embedding_dropout = nn.Dropout(p=embedding_dropout)
        self.target_sig = target_sig
        ### Star rating classifier
        
        star_rating_n_classes = 5
        self.hidden2out = nn.Linear(self.hidden_size, star_rating_n_classes)

        if rnn_type == 'rnn':
            rnn = nn.RNN
        elif rnn_type == 'gru':
            rnn = nn.GRU
        # elif rnn_type == 'lstm':
        #     rnn = nn.LSTM
        else:
            raise ValueError()

        self.encoder_rnn = rnn(embedding_size+self.cond1_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)
        self.decoder_rnn = rnn(embedding_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)
        self.cond_decoder_rnn = rnn(embedding_size+self.cond1_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)
        
        self.cond_neg_decoder_rnn = rnn(embedding_size+self.cond1_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)
        self.cond_pos_decoder_rnn = rnn(embedding_size+self.cond1_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)
        
        
        self.discriminator_rnn = rnn(embedding_size, hidden_size, num_layers=num_layers, bidirectional=self.bidirectional, batch_first=True)

        self.hidden_factor = (2 if bidirectional else 1) * num_layers
        self.softmax = nn.Softmax(dim=1)
        self.word_softmax= nn.Softmax(dim=-1)

        self.hidden2mean = nn.Linear(hidden_size * self.hidden_factor, latent_size)
        self.hidden2logv = nn.Linear(hidden_size * self.hidden_factor, latent_size)

        # self.pre_linear = nn.Linear(in_features=1, out_features=latent_size)
        self.pre_linear = nn.Linear(1, latent_size)
        self.cond_latent2hidden = nn.Linear(2 *latent_size, hidden_size * self.hidden_factor)
        self.outputs2vocab = nn.Linear(hidden_size * (2 if bidirectional else 1), vocab_size)

    def forward(self, input_sequence, length, cond1_raw):

        batch_size = input_sequence.size(0)
        sorted_lengths, sorted_idx = torch.sort(length, descending=True)
        input_sequence = input_sequence[sorted_idx]

        # ENCODER
        input_embedding = self.embedding(input_sequence)
        max_seq_len = input_embedding.shape[1]

        # cond1_raw= 100*(cond1_raw-2.5)
        cond1 = cond1_raw.type(torch.cuda.FloatTensor).unsqueeze(-1)
        cond1_rnn = cond1.repeat(1,self.max_sequence_length).unsqueeze(-1)
        input_cond = torch.cat((input_embedding, cond1_rnn), 2) 
        
        packed_input = rnn_utils.pack_padded_sequence(input_cond, sorted_lengths.data.tolist(), batch_first=True)

        _, hidden = self.encoder_rnn(packed_input)

        if self.bidirectional or self.num_layers > 1:
            # flatten hidden state
            hidden = hidden.view(batch_size, self.hidden_size*self.hidden_factor)
        else:
            hidden = hidden.squeeze()

        # mean = self.hidden2mean(hidden)  + cond1_raw.type(torch.cuda.FloatTensor).repeat(16,1).t()
        mean = self.hidden2mean(hidden)  
        logv = self.hidden2logv(hidden)
        std = torch.exp(0.5 * logv)
        
        # print(mean.size()) 

        z = self.target_sig * to_var(torch.randn([batch_size, self.latent_size]))
        z = z * std + mean
        
        z = z.type(torch.cuda.FloatTensor)
        # ipdb.set_trace()
        # REPARAMETERIZATION
        
        cond1 = cond1_raw.type(torch.cuda.FloatTensor).unsqueeze(-1)
        # extended_cond1 = self.pre_linear(cond1)
        extended_cond1 = 10*cond1.repeat(1, self.latent_size)
        z_cond = torch.cat((z, extended_cond1), 1)
        hidden = self.cond_latent2hidden(z_cond)

        if self.bidirectional or self.num_layers > 1:
            # unflatten hidden state
            hidden = hidden.view(self.hidden_factor, batch_size, self.hidden_size)
        else:
            hidden = hidden.unsqueeze(0)

        # decoder input
        if self.word_dropout_rate > 0:
            # randomly replace decoder input with <unk>
            prob = torch.rand(input_sequence.size())
            if torch.cuda.is_available():
                prob=prob.cuda()
            prob[(input_sequence.data - self.sos_idx) * (input_sequence.data - self.pad_idx) == 0] = 1
            decoder_input_sequence = input_sequence.clone()
            decoder_input_sequence[prob < self.word_dropout_rate] = self.unk_idx
            input_embedding = self.embedding(decoder_input_sequence)

        input_embedding = self.embedding_dropout(input_embedding)
        # packed_input = rnn_utils.pack_padded_sequence(input_embedding, sorted_lengths.data.tolist(), batch_first=True)
        # ipdb.set_trace()
        input_cond_dec = torch.cat((input_embedding, cond1_rnn), 2) 

        packed_input = rnn_utils.pack_padded_sequence(input_cond_dec, sorted_lengths.data.tolist(), batch_first=True)
        outputs, _ = self.cond_decoder_rnn(packed_input, hidden)
        padded_outputs = rnn_utils.pad_packed_sequence(outputs, batch_first=True)[0]
        padded_outputs = padded_outputs.contiguous()

        _, reversed_idx = torch.sort(sorted_idx)
        padded_outputs = padded_outputs[reversed_idx]
        b,s,_ = padded_outputs.size()

        word_output = self.outputs2vocab(padded_outputs.view(-1, padded_outputs.size(2)))
        logp = nn.functional.log_softmax(word_output, dim=-1)
        logp = logp.view(b, s, self.embedding.num_embeddings)

        dsc_word_output = word_output.reshape(input_embedding.shape[0], input_embedding.shape[1], -1)
        # dsc_word_output.requires_grad = True
        dsc_word_embedding = self.embedding(torch.max(dsc_word_output, dim=2)[1]) 
        packed_input_dsc = rnn_utils.pack_padded_sequence(dsc_word_embedding, sorted_lengths.data.tolist(), batch_first=True)
        dsc_rnn_outputs, dsc_hidden = self.discriminator_rnn(packed_input_dsc)
        disc_outputs_last = dsc_hidden[-1].type(torch.cuda.FloatTensor)
        disc_outputs = self.hidden2out(disc_outputs_last)
        c_hat = self.softmax(disc_outputs)

        return logp, mean, logv, z, disc_outputs, c_hat
    
    def DSC_net(self, dsc_word_output, sorted_lengths):
        dsc_word_embedding = self.embedding(dsc_word_output)
        packed_input_dsc = rnn_utils.pack_padded_sequence(dsc_word_embedding, sorted_lengths.data.tolist(), batch_first=True)
        dsc_rnn_outputs, dsc_hidden = self.discriminator_rnn(packed_input_dsc)
        disc_outputs_last = dsc_hidden[-1].type(torch.cuda.FloatTensor)
        disc_outputs = self.hidden2out(disc_outputs_last)
        c_hat = self.softmax(disc_outputs)

        return c_hat

    def inference(self, n=4, z=None, cond1=None, margin=4, dsc_trim_ratio=0.25, refining=False, topn=50, alpha=0.5):
        cond1_raw = cond1
        if z is None:
            batch_size = n
            z = to_var(torch.randn([batch_size, self.latent_size]))
        else:
            batch_size = z.size(0)
            n = batch_size
        
        if cond1 is None:
            cond1=1

        cond1 = torch.from_numpy(np.array([cond1])) 
        cond1 = cond1.type(torch.cuda.FloatTensor).unsqueeze(-1)
        z = z.type(torch.cuda.FloatTensor)
        cond1 = cond1.repeat(z.shape[0], 1)
        # z = z + cond1
        # extended_cond1 = self.pre_linear(cond1)
        extended_cond1 = 10*cond1.repeat(1, self.latent_size)
        z_cond = torch.cat((z, extended_cond1), 1)
        hidden = self.cond_latent2hidden(z_cond)


        if self.bidirectional or self.num_layers > 1:
            # unflatten hidden state
            hidden = hidden.view(self.hidden_factor, batch_size, self.hidden_size)

        hidden = hidden.unsqueeze(0)

        # required for dynamic stopping of sentence generation
        sequence_idx = torch.arange(0, batch_size, out=self.tensor()).long() # all idx of batch
        sequence_running = torch.arange(0, batch_size, out=self.tensor()).long() # all idx of batch which are still generating
        sequence_mask = torch.ones(batch_size, out=self.tensor()).byte()

        running_seqs = torch.arange(0, batch_size, out=self.tensor()).long() # idx of still generating sequences with respect to current loop

        generations = self.tensor(batch_size, self.max_sequence_length).fill_(self.pad_idx).long()

        t=0
        cond1_raw = torch.tensor(cond1_raw).type(torch.cuda.FloatTensor).unsqueeze(-1)
        while(t<self.max_sequence_length and len(running_seqs)>0):

            if t == 0:
                input_sequence = to_var(torch.Tensor(batch_size).fill_(self.sos_idx).long())

            input_sequence = input_sequence.unsqueeze(1)
            input_embedding = self.embedding(input_sequence)
            
            cond1 = cond1_raw.repeat(input_embedding.shape[0], 1, 1)
            dec_input = torch.cat((input_embedding, cond1), 2) 
            output, hidden = self.cond_decoder_rnn(dec_input, hidden)
            
            logits = self.outputs2vocab(output)
            _input_sequence = self._sample(logits)
            print("t : ", t)
            input_sequence = self._cond_sample(cond1, t, generations, logits, 
                                              running_seqs, refining, topn=topn, alpha=alpha)

            # save next input
            generations = self._save_sample(generations, input_sequence, sequence_running, t)
            # update gloabl running sequence
            sequence_mask[sequence_running] = (input_sequence != self.eos_idx).data
            sequence_running = sequence_idx.masked_select(sequence_mask)

            # update local running sequences
            running_mask = (input_sequence != self.eos_idx).data
            running_seqs = running_seqs.masked_select(running_mask)

            if len(running_seqs) > 0:
                # ipdb.set_trace()
                # running_seqs = running_seqs
                tp = input_sequence.max().long().item() 
                try:
                    input_sequence = input_sequence[running_seqs]
                except:
                    input_sequence = input_sequence.unsqueeze(-1)
                    # ipdb.set_trace()
                hidden = hidden[:, running_seqs]

                running_seqs = torch.arange(0, len(running_seqs), out=self.tensor()).long()

            t += 1
        if refining == False: 
            gen, sorted_lengths = self.get_sorted_gens(generations) 
            chat = self.DSC_net(gen, sorted_lengths)
            generations = self.compare_cond(gen, chat, cond1, dsc_trim_ratio)
            return generations, z
        else: 
            sorted_z, gen, sorted_lengths = self.get_sorted_z_and_gens(z, generations) 
            filter_n = max(int(n/np.sqrt(margin)), 3)
            refined_z = sorted_z[:filter_n, :]
            return gen[:filter_n], refined_z
    
    def get_sorted_gens(self, generations):
        sorted_lengths = self.get_sorted_length(generations)
        sorted_lengths = torch.tensor(sorted_lengths, dtype=torch.int).cuda()
        _, sorted_idx = torch.sort(sorted_lengths, descending=True)
        gen, sorted_lengths = generations[sorted_idx], sorted_lengths[sorted_idx]
        return gen, sorted_lengths
    
    def get_sorted_z_and_gens(self, z, generations):
        sorted_lengths = self.get_sorted_length(generations)
        sorted_lengths = torch.tensor(sorted_lengths, dtype=torch.int).cuda()
        _, sorted_idx = torch.sort(sorted_lengths, descending=True)
        z, gen, sorted_lengths =z[sorted_idx], generations[sorted_idx], sorted_lengths[sorted_idx]
        return z, gen, sorted_lengths

    def compare_cond(self, gen, chat, cond1, dsc_trim_ratio, thr=0.95):
        out_int = (chat.max(1)[1]+1).detach().cpu().numpy()
        # cond_int = cond1.squeeze().squeeze().detach().cpu().numpy().astype(int)
        bool = out_int == cond1[0] 
        # bool_thr = (chat[bool[0]].max(1)[0] > thr)
        val, idx = (chat[bool[0]].max(1)[0]).sort()
        trim_idx = idx[:int(gen.shape[0]*dsc_trim_ratio)]
        gen_po = gen[bool[0]]
        return gen_po[trim_idx]
        
    def get_sorted_length(self, gen):
        gen_len = gen.shape[0]
        max_len = gen.shape[1]
        sorted_lengths = []
        for k in range(gen_len):
            cot = (gen[k, :] == 0).nonzero().shape[0]
            if cot == 0:
                sorted_lengths.append(max_len)
            else:
                sorted_lengths.append((gen[k, :] == 0).nonzero().detach().cpu().numpy()[0][0])
        return sorted_lengths
    
    def _cond_sample(self, cond1, t, generations, dist, running_seqs, refining, 
                    mode='greedy', topn=50, alpha=0.5):
        # dist = self.
        dist_sm = self.word_softmax(dist)
        gen_run= generations[running_seqs, :]
        prob, idxm = torch.topk(dist_sm, topn, dim=-1)
        sample = torch.zeros((gen_run.shape[0], 1), dtype=torch.long).cuda()
        for i in range(gen_run.shape[0]):
            print(f"topn: {topn}, alpha: {alpha}")
            if t == 0:
                idxp = poten = idxm[i,:,:].t()
            else:
                genp = gen_run[i,:t].repeat(topn, 1)
                idxp = idxm[i, :, :].t()
                poten = torch.cat((genp, idxp), 1)

            gen, sorted_lengths = self.get_sorted_gens(poten)
            chat = self.DSC_net(gen, sorted_lengths)
            decision = chat.max(1)[1]
            cond_prob = chat[:, int(cond1[0])-1]
            # alpha = alpha* torch.tensor(np.exp(-1*(t/100)), dtype=torch.float).cuda()
            # ipdb.set_trace()
            total_prob = alpha * cond_prob + (1-alpha) * prob[i, :]
            cam = torch.argmax(total_prob)
            if refining == True and decision[cam] != int(cond1[0]-1):
                sample[i, 0] = self.eos_idx
            else:
                sample[i, 0] = idxp[cam]

        sample = sample.squeeze()
        return sample

        
    def _sample(self, dist, mode='greedy'):

        if mode == 'greedy':
            _, sample = torch.topk(dist, 1, dim=-1)
        sample = sample.squeeze()

        return sample

    def _save_sample(self, save_to, sample, running_seqs, t):
        # select only still running
        running_latest = save_to[running_seqs]
        # update token at position t
        running_latest[:,t] = sample.data
        # save back
        save_to[running_seqs] = running_latest

        return save_to

