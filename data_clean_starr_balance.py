# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import re
import ipdb
import os
from pathlib import Path
import argparse
import numpy as np
import random 

# reload(sys)
os.environ["PYTHONIOENCODING"] = "utf-8"

def write_txt(path, the_list): 
    outF = open(path, "w")
    for line in the_list:
        outF.write(line)
        outF.write("\n")
    outF.close()


def read_txt(list_path):
    with open(list_path, encoding="utf-8") as f:

        content = []
        for line in f:
            line = line.encode('ascii', 'ignore').decode('ascii')
            line = line.strip("\n")
            # print(line) 
            content.append(line)
    return content

def get_split_key(split_dict, k):
    if 0 <= k < split_dict["train"]:
        return "train"
    elif split_dict["train"] <= k < split_dict["valid"]:
        return "valid"
    elif split_dict["valid"] <= k < split_dict["test"]:
        return "test"
    else:
        raise ValueError("{} does not belong to any category.".format(k))

def get_starr_split_key(star_rating, starr_split_dict, k):
    if 0 <= k < starr_split_dict[star_rating]["train"]:
        return "train"
    elif starr_split_dict[star_rating]["train"] <= k < starr_split_dict[star_rating]["valid"]:
        return "valid"
    elif starr_split_dict[star_rating]["valid"] <= k < starr_split_dict[star_rating]["test"]:
        return "test"
    else:
        raise ValueError("{} does not belong to any category.".format(k))

def text_clean(line, unwanted):
    # line = line.encode('utf-8').decode('utf-8', 'ignore')
    line = line.replace("<br />", "")

    puncs = [".", ",", "!", "/"]
    for punc in puncs:
        line = line.replace(punc, " {} ".format(punc))
    # print(line)
    resultwords = line.lower().split()
    # resultwords  = ([word.lower() for word in re.split("\W+",line) if word.lower() not in unwanted])

    return ' '.join(resultwords), len(resultwords)

def check_sentiment(line_item):
    foo = line_item.split()
    for line in foo:
        if any(x in line for x in ["disappointed", "return", "bad", "doesn't", "don't", "worst", "junk", "crap"]):
            sentiment = "1"
        elif any(x in line for x in ["good", "glad", "great", "nice", "happy","loves", "love", "best"]):
            sentiment = "5"
        else:
            sentiment = "?"

    return sentiment

def get_col_dict(cont_list, dict_out, split_dict, star_ratio, item_names_list, unwanted, force_balanced_rating=True):
# def get_col_dict(cont, dict_out, starr_split_dict, item_names_list, unwanted, force_balanced_rating=True):
    cont_col_names = cont_list[0]
    dict_keys = cont_col_names.split("\t")
    cont_body = cont_list[1:]

    count = {"train": 0, "test": 0, "valid": 0}
    for k, line_raw in enumerate(cont_body):
        if (k) % int(len(cont_body)/10) == 0:
            print("Reading {}/{}".format(k, len(cont_body)))
        
        line = line_raw.split("\t")
        rev_body_len = len( line[dict_keys.index("review_body")].split())
        star_rating = int(line[dict_keys.index("star_rating")])
        split_key = get_split_key(split_dict, k)
        # split_key = get_starr_split_key(star_rating, starr_split_dict, k)
        if force_balanced_rating:
            if star_rating in [2,3,4]:
                continue
        if rev_body_len < 15:
            continue
        elif rev_body_len > 60:
            line[dict_keys.index("review_body")] = ' '.join(line[dict_keys.index("review_body")].split()[:60])
        
        setm = check_sentiment(line[dict_keys.index("review_body")])
        star_rating = line[dict_keys.index("star_rating")]
        # if setm != star_rating and setm != '?':
            # print("setm:", setm)
            # print("star_rating:", star_rating)
            # print(line[dict_keys.index("review_body")])

        for i, item_names_tup in enumerate(item_names_list):
            (key, data_type) = item_names_tup
            assert key in dict_keys, "The item name {} is not in the data".format(key)
            idx = dict_keys.index(key)
            line_item, line_length = text_clean(line[idx], unwanted)
            if key not in dict_out:
                print("Creating key:", key)
                dict_out[key] = {split_key: [ line_item ] }
            else:
                if split_key not in dict_out[key]:
                    dict_out[key][split_key] = [ line_item ]
                else:
                    dict_out[key][split_key].append(line_item)

    print("Balancing the star ratings... ")
    dict_out = balance_star_rating(dict_out, star_ratio)
    return dict_out

def balance_star_rating(dict_out, star_ratio):
    
    star_rating_count_dict = {'train':{}, 'test':{}, 'valid':{}}
    star_balanced_bool_dict = {}
    new_dict_out = {}
    split_dict = dict_out['star_rating']
    
    for split_key, val in split_dict.items():
        print("counting split:", split_key)
        star_rating_np = np.array(val)
        for star in ['1', '2', '3', '4', '5']:
            star_rating_count_dict[split_key][star] = star_rating_np == star

        anchor_bool = star_rating_count_dict[split_key]['1']
        count_anchor = np.count_nonzero(anchor_bool)
        reduced_index_count = int(count_anchor * star_ratio['5']/star_ratio['1']) 
        mod_bool_vec = star_rating_count_dict[split_key]['5']
        new_reduced_bool = np.where(mod_bool_vec == True)[0]
        np.random.shuffle(new_reduced_bool)
        
        new_reduced_bool_crop = new_reduced_bool[:reduced_index_count]
        mod_bool_vec[:] = False
        mod_bool_vec[new_reduced_bool_crop] = True

        star_balanced_bool_dict[split_key] = np.logical_or(mod_bool_vec, anchor_bool)
     
    for item_name, split_dict in dict_out.items():
        new_split_dict = {}
        for split_key, split_list in split_dict.items():
            print("balancing:", item_name, " split:", split_key)
            split_np = np.array(split_list)
            balanced_bool = star_balanced_bool_dict[split_key]
            split_list = list(split_np[balanced_bool])
            new_split_dict[split_key] = split_list
        new_dict_out[item_name] = new_split_dict
    return new_dict_out

def shuffle_cont(cont_list):
    cont_index = cont_list[0]
    cont_body = cont_list[1:]
    random.shuffle(cont_body)
    return [cont_index] + cont_body



if __name__ == "__main__":

    '''
    Data cleaning script.

    argv[1]: the full path of the input file
    argv[2]: the full path of the output folder
    ex: python3 data_clean amazon_reviews_us_Musical_Instruments_v1_00 ./data_folder/
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str)
    args = parser.parse_args() 
    
    random.seed(1)
    # full_tsv_file_path = sys.argv[1]
    # full_tsv_file_out_path = sys.argv[2]

    home_path = str(Path.home())
    
    # data_base_path = "/raid0_8tb/datasets/amazon_review_data/toy_data2"
    # dataset_list = ["amazon_reviews_us_Musical_Instruments_v1_00.tsv",
                    # "amazon_reviews_us_Lawn_and_Garden_v1_00.tsv"]
    # full_tsv_file_path_list = [ "{}/{}".format(data_base_path, x) for x in dataset_list ]
    # full_tsv_file_out_path = data_base_path
   
    data_setup = "full"
    if data_setup =="1":
        data_base_path = "{}/amazon_data".format(home_path)
        dataset_list = ["amazon_reviews_us_Musical_Instruments_v1_00.tsv"]
    elif data_setup == "full":
        # data_base_path = args.data_dir
        data_base_path = "{}/amazon_data".format(home_path)
        # dataset_list = read_txt("gz_list.txt")
        dataset_list = read_txt("gz_list_subset1.txt")
    else:
        raise ValueError(f"{data_setup} is not appropriate setup.")

    full_tsv_file_path_list = [ "{}/{}".format(data_base_path, x) for x in dataset_list ]
    full_tsv_file_out_path = data_base_path

    ### marketplace customer_id review_id   product_id  product_parent  product_title   product_category    star_rating helpful_votes   total_votes vine    verified_purchase   review_headline review_body review_date
    item_names_list = [("review_body", ""), 
                       ("star_rating", ".star_rating"),
                       ("product_title", ".product_title") ]

    
    total_train = int(1e+7)
    total_train = 904766
    truncate_len = total_train

    ### Ratio of star rating
    star_ratio = {'1':1.0, '5':0.5}
    val_ratio, test_ratio = 0.1, 0.1

    # minmax_sent_len = (5, 60)
    itemized_data_dict = {}
    for i, full_tsv_file_path in enumerate(full_tsv_file_path_list):
        print("i:{}/{}".format(i, len(full_tsv_file_path_list)), f" Processing data : {full_tsv_file_path}")

        data_tag = full_tsv_file_path.split("/")[-1].split("_")[0]
        cont_list = read_txt(full_tsv_file_path) 

        # cont_list = cont_list[:(truncate_len+1)]
        # cont_list = shuffle_cont(cont_list) 
       
        split_dict = {"valid": int(len(cont_list) * (1-test_ratio)),
                      "test": int(len(cont_list)),
                      "train": int(len(cont_list) * (1-val_ratio-test_ratio)  )}
        
        # starr_split_dict = {"valid": {1: starr[1] * (1-test_ratio), 5:starr[5]*(1-test_ratio)},
                            # "test":  {1: starr[1], 5:starr[5]},
                            # "train": {1: starr[1] * (1-val_ratio-test_ratio), 5:starr[5]*(1-val_ratio-test_ratio) }
                            # }
        
        print("Adding dataset:", star_ratio)

        ### Specify the unwanted strings 
        unwanted = ["<br />"]

        ### Organize the dataset into "itemized_data_dict"
        # itemized_data_dict = get_col_dict(cont_list, itemized_data_dict, max_line, minmax_sent_len, item_names_list, split, unwanted)
        itemized_data_dict = get_col_dict(cont_list, 
                                        itemized_data_dict, 
                                        split_dict, 
                                        star_ratio,
                                        item_names_list, 
                                        unwanted, 
                                        force_balanced_rating=True)
   
    print("Writing files")
    for (item_name, data_type) in item_names_list:
        for split_name in ["train", "valid", "test"]:
            write_txt("{}/{}".format(full_tsv_file_out_path, "{}.{}{}.txt".format(data_tag, split_name, data_type)), 
                      itemized_data_dict[item_name][split_name])
