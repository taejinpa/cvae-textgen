# How to run

#### First git clone
git clone https://taejinpa@bitbucket.org/taejinpa/cvae-textgen.git

#### Create a virtual environment
virtualenv -p $(which python3) env_cvae

#### Install requirements
(env_cvae)$ pip3 install -r requirements.txt

#### Download and clean the data to $HOME/amazon_data
sh download_and_prep_data.sh

#### Model Training
sh train.sh

#### Text Generation
sh gen_text.sh <model_epoch> <number_of_samples> <star_rating_1_or_5> <alpha> <topn> <seed>
ex) sh gen_text.sh 1 10 1 0.65 10 2


This repository is based on the following repository:
https://github.com/timbmg/Sentence-VAE/
