
DATA_FOLDER="$HOME/amazon_data"

mkdir -p $DATA_FOLDER
wget https://s3.amazonaws.com/amazon-reviews-pds/tsv/amazon_reviews_us_Electronics_v1_00.tsv.gz
wget https://s3.amazonaws.com/amazon-reviews-pds/tsv/amazon_reviews_us_Mobile_Electronics_v1_00.tsv.gz
wget https://s3.amazonaws.com/amazon-reviews-pds/tsv/amazon_reviews_us_Camera_v1_00.tsv.gz
wget https://s3.amazonaws.com/amazon-reviews-pds/tsv/amazon_reviews_us_Home_Entertainment_v1_00.tsv.gz
wget https://s3.amazonaws.com/amazon-reviews-pds/tsv/amazon_reviews_us_Major_Appliances_v1_00.tsv.gz

gunzip amazon_reviews_us_Electronics_v1_00.tsv.gz
gunzip amazon_reviews_us_Mobile_Electronics_v1_00.tsv.gz
gunzip amazon_reviews_us_Camera_v1_00.tsv.gz
gunzip amazon_reviews_us_Home_Entertainment_v1_00.tsv.gz
gunzip amazon_reviews_us_Major_Appliances_v1_00.tsv.gz

mv *.tsv $DATA_FOLDER/

python3 data_clean_starr_balance.py --data_dir $DATA_FOLDER

